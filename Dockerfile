FROM python:3.9-bullseye

RUN apt-get update && \
    apt-get install -y \
        apt-utils \
        git \
        gcc \
        libjpeg62-turbo-dev \
        zlib1g-dev \
        openssh-server \
        gettext \
        libxml2-dev \
        libxslt-dev \
        libssl-dev \
        swig \
        jpegoptim \
        redis-tools \
        supervisor \
        nano \
        snmp \
        tzdata \
        locales

RUN mkdir /app \
          /var/log/app

RUN touch /var/log/app/debug.log && \
    chmod 777 /var/log/app/debug.log

# Avoid cache purge by adding requirements first

COPY requirements/common.txt /tmp/common.txt
COPY requirements/local.txt /tmp/local.txt
RUN pip install pip==19.3.1
RUN pip install -r /tmp/local.txt

WORKDIR /app/
