import datetime
from typing import List

from asgiref.sync import sync_to_async
from django.utils import timezone


from .base_subprocess import Command as BaseCommand

from subscribe.models import PremiumUser
from constance import config


class SubscribeProcessor(object):

    @staticmethod
    def user_processing(user: PremiumUser):
        current_date = timezone.now()

        if user.is_paid and user.subscription_duration:
            expiration_date = user.created_at + user.subscription_duration
            if current_date > expiration_date:
                user.is_paid = False
                user.save()

        elif not user.is_paid and not user.subscription_duration:
            delete_date = user.created_at + datetime.timedelta(days=config.NOT_PAID_DAYS)
            if current_date > delete_date:
                user.delete()

        elif not user.is_paid and user.subscription_duration:
            delete_date = user.created_at + user.subscription_duration + datetime.timedelta(days=config.NOT_PAID_DAYS)
            if current_date > delete_date:
                user.delete()

    def processing(self, users: List[PremiumUser]):
        for user in users:
            self.user_processing(user)


class Command(BaseCommand):
    help = 'Check premium users by expired subscriptions'

    command_name = __name__.rsplit('.', 1)[-1]

    delay = 60

    subscribe_processor = None

    max_users = 50

    @sync_to_async
    def processing(self):
        users = PremiumUser.objects.all()[:self.max_users]
        self.subscribe_processor.processing(users)

    def before_start(self):
        self.subscribe_processor = SubscribeProcessor()

    async def do_action(self, context):
        await self.processing()
