import os
import signal
import subprocess
import sys
import time
import asyncio

from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils.translation import gettext_lazy

import logging
debug_logger = logging.getLogger('logfile_debug')
error_logger = logging.getLogger('logfile_error')


class Command(BaseCommand):
    # Metadata about this command.
    help = gettext_lazy("Base command as a subprocess")

    PID_DIR = 'run'
    MAX_COMPLETION_TIMEOUT = 10
    COMPLETION_CHECK_INTERVAL = 1

    # таймаут основного цикла корректируется (уменьшается) автоматически в зависимости от времени выполнения его тела,
    # минимум 0.01 сек
    MAIN_LOOP_TIMEOUT = 1

    pid = -1
    process_title = ''
    command_name = __name__.rsplit('.', 1)[-1]
    slot = None
    step = 0

    gather_and_run = None
    loop = None
    _main_loop_is_running = False
    _last_action_run_at = None
    _current_main_loop_timeout = 0

    lock = None

    REDIS_PARAM_PREFIX = 'entity:update'

    DEFAULT_SLOT = '0'

    def add_arguments(self, parser):
        parser.add_argument(
            '--start',
            dest='status',
            action='store_true',
            default=None,
            help=gettext_lazy('Start process')
        )
        parser.add_argument(
            '--stop',
            dest='status',
            action='store_false',
            default=None,
            help=gettext_lazy('Stop process')
        )
        parser.add_argument(
            '--run',
            dest='run',
            action='store_true',
            help=gettext_lazy('Run subprocess')
        )
        parser.add_argument(
            '--slot',
            type=str,
            dest='slot',
            default=self.DEFAULT_SLOT,
            help=gettext_lazy('Command slot')
        )

    def is_running(self):
        return self._main_loop_is_running

    @classmethod
    def get_pid_file_name(cls, process_name, slot):
        """
        Возвращает имя файла процесса
        """
        return f'{process_name}_{slot}.pid'

    @classmethod
    def get_pid_file_path(cls, pid_file_name):
        """
        Возвращает полный путь к файлу процесса, и создает директорию в случаи отсутствия
        """
        path = os.path.join(settings.PROJECT_PATH, cls.PID_DIR)
        if not os.path.exists(path):
            os.mkdir(path)
        return os.path.join(path, pid_file_name)

    @classmethod
    def get_process_title(cls, command_name=''):
        """
        Возвращает наименование процесса
        """
        if not command_name:
            command_name = cls.get_command_name()
        return command_name.replace('_', ' ')

    @classmethod
    def get_command_name(cls):
        """
        Возвращает имя текущей команды
        """
        return cls.command_name

    @classmethod
    def get_pid(cls, pid_file_path):
        """
        Возвращает id запущенного процесса, либо -1 если файл процесса не найден
        """
        pid = -1
        try:
            with open(pid_file_path, 'r') as pid_file:
                pid = int(pid_file.read().strip())
        finally:
            return pid

    def ask_exit(self):
        self._main_loop_is_running = False

    def _hard_ask_exit(self):
        self.ask_exit()
        for task in asyncio.all_tasks(self.loop):
            task.cancel()
        try:
            pid_file_path = self.get_pid_file_path(self.get_pid_file_name(self.command_name, self.slot))
            debug_logger.debug(f"{self.process_title} os.remove pid file {pid_file_path}")
            os.remove(pid_file_path)
        except OSError:
            error_logger.error(f"{self.process_title} OSError")

    async def do_action(self, context):
        """
        Тело основного цикла
        """
        error_logger.error(f'ping {self.process_title} process [pid:{self.pid}] [slot:{self.slot}]')
        # pass

    def before_start(self):
        """
        Инициализация процесса до запуска основного цикла
        """
        pass

    async def after_start(self, context):
        """
        Инициализация процесса после запуска основного цикла
        """
        pass

    async def before_stop(self, context):
        """
        Завершение процесса до остановки основного цикла
        """
        debug_logger.debug(f"{self.process_title} before_stop")
        pass

    def after_stop(self):
        """
        Завершение процесса после остановки основного цикла
        """
        debug_logger.debug(f"{self.process_title} after_stop")
        pass

    async def _main_loop(self):
        iterator = 0
        context = {}
        await self.after_start(context)
        self._last_action_run_at = time.time() - self.get_main_loop_timeout(context)
        while self.is_running():
            iterator+=1
            try:
                self._current_main_loop_timeout = min(
                    max(self.get_main_loop_timeout(context) + self._last_action_run_at - time.time(), 0.01),
                    self.MAIN_LOOP_TIMEOUT
                )
                if iterator % 1000 == 0:
                    debug_logger.debug(f"{self.process_title} _main_loop {iterator} {self._current_main_loop_timeout}")

                await asyncio.sleep(self._current_main_loop_timeout)
            except asyncio.CancelledError:
                error_logger.error(f"{self.process_title} {iterator} asyncio.CancelledError")
                break
            except asyncio.TimeoutError:
                error_logger.error(f"{self.process_title} {iterator} asyncio.TimeoutError")
            except Exception as err:
                error_logger.error(f"MAIN LOOP EXCEPTION {iterator}: {err}")
            else:
                self._last_action_run_at = time.time()
                await self.do_action(context)
        await self.before_stop(context)
        self.loop.stop()

    @property
    def current_main_loop_timeout(self):
        return self._current_main_loop_timeout

    def get_main_loop_timeout(self, context):
        """
        Позволяет менять таймаут основного цикла
        """
        return self.MAIN_LOOP_TIMEOUT

    @staticmethod
    def get_subprocess_command_line_arguments(*args, **options):
        """
        Позволяет "пробрасывать" аргументы в подпроцесс

        Пример:

        @staticmethod
        def get_subprocess_command_line_arguments(*args, **options):
            return [f'-s={options.get("spam")}']

        def add_arguments(self, parser):
            # add some args ...
            parser.add_argument(
                '-s',
                '--spam',
                type=str,
                dest='spam',
                default='',
                help=gettext_lazy('Spam...')
            )
            super().add_arguments(parser)

        """
        return []

    def check_pid_process(self, pid):
        try:
            readlink_subproc = subprocess.run(
                ['ps', "-p", f"{pid}", "--format", "stat"],
                check=True,
                capture_output=True)

            res = readlink_subproc.stdout.decode('utf-8').split('\n')
            if res and not res[1].startswith('Z'):
                return True
        except Exception as err:
            error_logger.error(f"check_pid_process {self.process_title} run: {pid} not running: {err}")

        return False

    def handle(self, *args, **options):
        """
        Обработчик команды
        """

        run = options.get('run')
        status = options.get('status')
        self.slot = options.get('slot')

        self.command_name = self.get_command_name()
        self.process_title = self.get_process_title(command_name=self.command_name)

        debug_logger.debug(f"HANDLE COMMAND {self.process_title} run: {run} status: {status}")
        pid_file_path = self.get_pid_file_path(self.get_pid_file_name(self.command_name, self.slot))
        self.pid = self.get_pid(pid_file_path)
        debug_logger.debug(f"{self.process_title} pid: {self.pid}")
        if run:
            # orphaned process check
            pid = os.getpid()
            debug_logger.debug(f"RUN {self.process_title} os.pid: {pid}")
            if self.pid == pid:
                self.stdout.write(self.style.SUCCESS(f'run {self.process_title}'.capitalize()))
                debug_logger.debug(f'run {self.process_title} is_running:{self.is_running()}')
                # Running an asyncio
                self.loop = asyncio.get_event_loop()

                for sig in (signal.SIGINT, signal.SIGTERM, signal.SIGILL):
                    self.loop.add_signal_handler(sig, self._hard_ask_exit)

                # If any awaitable in aws is a coroutine, it is automatically scheduled as a Task.
                self.gather_and_run = [self._main_loop]

                # run before start main loop
                self.before_start()

                # Schedule calls *concurrently*:
                for task in self.gather_and_run:
                    self.loop.create_task(task())

                # mutex lock for asyncio tasks
                self.lock = asyncio.Lock()

                self._main_loop_is_running = True
                try:
                    debug_logger.debug(f"{self.process_title} RUN try: run_forever {self.pid}")
                    # run_forever() returns after calling loop.stop()
                    self.loop.run_forever()

                    tasks = asyncio.all_tasks(self.loop)

                    for t in [t for t in tasks if not (t.done() or t.cancelled())]:
                        # give canceled tasks the last chance to run
                        self.loop.run_until_complete(t)

                finally:
                    debug_logger.debug(f"{self.process_title} RUN finally STOP {self.pid} step {self.step}")
                    self.loop.run_until_complete(self.loop.shutdown_asyncgens())

                    for sig in (signal.SIGINT, signal.SIGTERM, signal.SIGILL):
                        self.loop.remove_signal_handler(sig)

                    self.loop.close()

                # run on main loop stop
                self.after_stop()

                # mark process stopped
                with open(pid_file_path, 'w') as pid_file:
                    pid_file.write('-1')

                self.stdout.write(
                    self.style.SUCCESS(
                        f'{self.process_title} process [pid:{self.pid}] [slot:{self.slot}] stopped.'.capitalize()))
                debug_logger.debug(f"{self.process_title} step {self.step} RUN stopped")

            else:
                debug_logger.debug(f"{self.process_title} Try to run orphaned {pid}")
                self.stdout.write(
                    self.style.ERROR(f"""Try to run orphaned {self.process_title} process [pid:{pid}] [slot:{
                    self.slot}]""".capitalize()))

        elif status is None:
            debug_logger.debug(f"{self.process_title} step {self.step} status is None {self.pid}")
            # If the command is executed without arguments, it's show current process status
            if self.pid != -1 and self.check_pid_process(self.pid):
                debug_logger.debug(f"{self.process_title} step {self.step} is running with process [pid:{self.pid}]")
                self.stdout.write(
                    self.style.NOTICE(f"""{self.process_title} is running with process [pid:{self.pid}] [slot:{
                    self.slot}]""".capitalize()))
            else:
                debug_logger.debug(f"{self.process_title} is not running")
                self.stdout.write(self.style.NOTICE(
                    f'{self.process_title} is not running [slot:{self.slot}].'.capitalize()))

        elif status:
            exec_pid = os.getpid()
            debug_logger.debug(f"START {self.process_title} step {self.step} status {status} {self.pid} / {exec_pid}")
            # If the command is executed with status=True it should start the subprocess
            if self.pid == -1 or not self.check_pid_process(self.pid):
                p = subprocess.Popen(['python', 'manage.py', self.command_name, '--run', f'--slot={self.slot}'] +
                                     self.get_subprocess_command_line_arguments(*args, **options))
                pid = str(p.pid)
                with open(pid_file_path, 'w') as pid_file:
                    pid_file.truncate(0)
                    pid_file.seek(0)
                    pid_file.write(pid)
                self.stdout.write(
                    self.style.SUCCESS(
                        f'{self.process_title} started with process [pid:{pid}] [slot:{self.slot}].'.capitalize()))
                debug_logger.debug(f"{self.process_title} started with process {pid}")
            else:
                debug_logger.debug(f"{self.process_title} step {self.step} ELSE already started with process {self.pid}")
                self.stdout.write(
                    self.style.ERROR(f"""{self.process_title} already started with process [pid:{self.pid}] [slot:{
                    self.slot}].""".capitalize()))

        else:
            exec_pid = os.getpid()
            debug_logger.debug(f"STOP {self.process_title} step {self.step} status=False pid: {self.pid} / {exec_pid}")
            # if the command is executed again with status=False, the process should be terminated.
            if self.pid != -1:
                # Terminate child process
                self.stdout.write(
                    self.style.NOTICE(
                        f'Sending Ctrl+C to the {self.process_title} process [pid:{self.pid}] [slot:{self.slot}].'))
                try:
                    debug_logger.debug(f"TRY KILL {self.process_title} step {self.step} Try kill {self.pid}, signal.SIGINT {signal.SIGINT}")
                    os.kill(self.pid, signal.SIGINT)
                except ProcessLookupError:
                    error_logger.error(f"KILL ProcessLookupError step {self.step} {self.process_title}")
                except Exception as err:
                    error_logger.error(f"KILL Exception step {self.step} {self.process_title}: {err}")
                else:
                    start = time.time()
                    debug_logger.debug(f"KILL START step {self.step} {self.process_title}: {start}")
                    while True:
                        # Keep waiting...
                        time.sleep(self.COMPLETION_CHECK_INTERVAL)

                        # Check the pid
                        if self.get_pid(pid_file_path) == -1:
                            debug_logger.debug(f"{self.process_title} kill Check the pid {self.pid} -> break")
                            break

                        # Check the timeout.
                        now = time.time()
                        if now - start > self.MAX_COMPLETION_TIMEOUT:
                            debug_logger.debug(f"MAX_COMPLETION_TIMEOUT {now} {self.process_title} step {self.step} Try kill {self.pid}, {signal.SIGKILL} -> break")
                            # We've waited too long.
                            self.stdout.write(
                                self.style.WARNING(
                                    f'Kill {self.process_title} process [pid:{self.pid}] [slot:{self.slot}].'))
                            try:
                                os.kill(self.pid, signal.SIGKILL)
                            except Exception as err:
                                error_logger.error(
                                    f"OS KILL EXEPTION {self.process_title} Try kill {self.pid}, {signal.SIGKILL} -> {err}")
                            break
                    debug_logger.debug(f"{self.process_title} step {self.step} killed")

            else:
                debug_logger.debug(f"{self.process_title} is not running")
                self.stdout.write(self.style.NOTICE(
                    f'{self.process_title} is not running [slot:{self.slot}].'.capitalize()))
            try:
                debug_logger.debug(f"{self.process_title} os.remove pid file {pid_file_path}")
                os.remove(pid_file_path)
            except OSError as err:
                error_logger.error(f"{self.process_title} OSError: {err}")

        debug_logger.debug(f"HANDLE COMMAND END -> step {self.step} {self.process_title}")
        sys.exit(0)
