import requests
import hashlib
import hmac
import json
from urllib.parse import urljoin

from constance import config

from django.conf import settings
from rest_framework.test import APITestCase
from rest_framework import status
from subscribe.models import PremiumUser


class SubscribeViewTest(APITestCase):
    def setUp(self):
        auth_service_url = urljoin(config.URL_TO_AUTH_SERVICE, 'api/get-token/')

        data = {
            'user_id': 1
        }

        message = json.dumps(data, separators=(',', ':')).encode('utf-8')
        calculated_hmac = hmac.new(settings.HMAC_KEY, message, hashlib.sha256).hexdigest()

        headers = {'Authorization': f'HMAC {calculated_hmac}'}
        response = requests.post(auth_service_url, json=data, headers=headers)

        self.token = response.json()['token']

    def test_subscribe_view_success(self):
        data = {"user_id": 1, "duration": "P30D"}

        headers = {'Authorization': f'Bearer {self.token}'}
        response = self.client.post('/api/subscribe/', data, format='json', headers=headers)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {'success': True})

    def test_subscribe_view_failure(self):
        data = {"user_id": 1, "duration": "invalid_duration"}
        headers = {'Authorization': f'Bearer {self.token}'}
        response = self.client.post('/api/subscribe/', data, format='json', headers=headers)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['success'], False)
        self.assertIn('message', response.data)


class IsSubscribedViewTest(APITestCase):

    def setUp(self):
        PremiumUser.objects.create(user_id=1, subscription_duration='P30D', is_paid=True)

        auth_service_url = urljoin(config.URL_TO_AUTH_SERVICE, 'api/get-token/')

        data = {
            'user_id': 1
        }

        message = json.dumps(data, separators=(',', ':')).encode('utf-8')
        calculated_hmac = hmac.new(settings.HMAC_KEY, message, hashlib.sha256).hexdigest()

        headers = {'Authorization': f'HMAC {calculated_hmac}'}
        response = requests.post(auth_service_url, json=data, headers=headers)

        self.token_1 = response.json()['token']

        data = {
            'user_id': 2
        }

        message = json.dumps(data, separators=(',', ':')).encode('utf-8')
        calculated_hmac = hmac.new(settings.HMAC_KEY, message, hashlib.sha256).hexdigest()

        headers = {'Authorization': f'HMAC {calculated_hmac}'}
        response = requests.post(auth_service_url, json=data, headers=headers)

        self.token_2 = response.json()['token']

    def test_is_subscribed_view_true(self):
        headers = {'Authorization': f'Bearer {self.token_1}'}
        response = self.client.get('/api/is_subscribed/?user_id=1', headers=headers)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {'is_subscribed': True})

    def test_is_subscribed_view_false(self):
        PremiumUser.objects.create(user_id=2, subscription_duration=None, is_paid=False)
        headers = {'Authorization': f'Bearer {self.token_2}'}
        response = self.client.get('/api/is_subscribed/?user_id=2', headers=headers)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {'is_subscribed': False})
