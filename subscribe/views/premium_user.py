from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi

from subscribe.contrib import SubscriptionManager
from subscribe.models import PremiumUser
from subscribe.rest.permissions import IsValidJWTTokenPermission
from subscribe.rest.serializers import SubscriptionSerializer


class SubscribeView(APIView):
    permission_classes = [IsValidJWTTokenPermission]

    @swagger_auto_schema(
        operation_description="Операция подписки на PremiumUser.",
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'user_id': openapi.Schema(type=openapi.TYPE_INTEGER, description='Идентификатор пользователя'),
                'duration': openapi.Schema(type=openapi.TYPE_STRING, description='Продолжительность подписки в ISO формате'),
            },
        ),
        responses={200: "Успешный ответ", 400: "Неверный запрос"},
    )
    def post(self, request, *args, **kwargs):
        serializer = SubscriptionSerializer(data=request.data)
        if serializer.is_valid():
            user_id = serializer.validated_data['user_id']
            duration = serializer.validated_data['duration']

            success = SubscriptionManager.subscribe(user_id, duration)

            if success:
                return Response({'success': True}, status=status.HTTP_200_OK)
            else:
                return Response({'success': False, 'message': 'Failed to subscribe'},
                                status=status.HTTP_400_BAD_REQUEST)
        return Response({'success': False, 'fields': serializer.errors, 'message': 'Failed to subscribe'},
                        status=status.HTTP_400_BAD_REQUEST)


class IsSubscribedView(APIView):
    permission_classes = [IsValidJWTTokenPermission]

    @swagger_auto_schema(
        operation_description="Операция проверки подписки пользователя.",
        responses={200: "Успешный ответ"},
    )
    def get(self, request, *args, **kwargs):
        user_id = request.query_params.get('user_id')
        is_subscribed = PremiumUser.objects.filter(user_id=user_id, is_paid=True).exists()
        return Response({'is_subscribed': is_subscribed}, status=status.HTTP_200_OK)
