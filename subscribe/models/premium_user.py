from django.db import models
from django.utils import timezone

from django.utils.translation import gettext_lazy as _


class PremiumUser(models.Model):
    user_id = models.PositiveIntegerField(
        verbose_name=_("User id"),
        null=False,
        blank=False,
        unique=True
    )

    subscription_duration = models.DurationField(
        verbose_name=_("Subscription duration"),
        null=True,
        blank=True
    )

    created_at = models.DateTimeField(
        verbose_name=_("Created at"),
        default=timezone.now
    )

    is_paid = models.BooleanField(
        verbose_name=_("Is paid"),
        default=False,
    )

    class Meta:
        verbose_name = _('Premium user')
        verbose_name_plural = _('Premium users')

    def __str__(self):
        return f"{self._meta.verbose_name} {self.user_id}"
