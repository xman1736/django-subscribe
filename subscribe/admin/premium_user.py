from django.contrib import admin
from subscribe.models import PremiumUser
from django.utils.translation import gettext_lazy as _


class PremiumUserAdmin(admin.ModelAdmin):
    list_display = ('user_id', 'subscription_duration', 'created_at', 'is_paid')
    search_fields = ('user_id',)
    list_filter = ('is_paid', 'created_at')
    ordering = ('-created_at',)
    readonly_fields = ('created_at',)

    fieldsets = (
        (None, {
            'fields': ('user_id', 'is_paid',)
        }),
        (_("Dates"), {
            'fields': ('created_at', 'subscription_duration',)
        }),
    )


admin.site.register(PremiumUser, PremiumUserAdmin)
