import datetime

from subscribe.models import PremiumUser


class SubscriptionManager(object):
    """
    Subscription manager designed for the subscription and payment process.
    """
    @classmethod
    def subscribe(cls, user_id: int, duration: datetime.timedelta) -> bool:
        """
        Subscription and payment process.

        Args:
            user_id (int): User identifier for whom the subscription is made.
            duration (datetime.timedelta): Subscription duration represented by a timedelta object.

        Returns:
            bool: True if the payment was successful and the subscription was created. False otherwise.
        """
        payment_success = cls.do_payment()

        user, is_created = PremiumUser.objects.get_or_create(
            user_id=user_id,
            defaults={
                'user_id': user_id
            }
        )

        if payment_success:
            if is_created:
                user.subscription_duration = duration
            else:
                user.subscription_duration += duration
            user.is_paid = True
            user.save()

            return True
        else:
            return False

    @classmethod
    def do_payment(cls, *args, **kwargs) -> bool:
        """
        Payment process.

        Returns:
            bool: Result of successful completion of the payment.
        """
        return True

