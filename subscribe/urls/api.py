from django.urls import path
from subscribe.views import SubscribeView, IsSubscribedView

urlpatterns = [
    path('subscribe/', SubscribeView.as_view(), name='subscribe'),
    path('is_subscribed/', IsSubscribedView.as_view(), name='is_subscribed'),
]
