from rest_framework import serializers


class SubscriptionSerializer(serializers.Serializer):
    user_id = serializers.IntegerField()
    duration = serializers.DurationField()
